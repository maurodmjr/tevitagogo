SHELL := /bin/bash

dev:
	export DATABASE_PASSWORD=Admin123! && \
	export DATABASE_PORT=3306 && \
	export DATABASE_NAME=telagogodb

black:
	black .

run:
	export FLASK_APP=run.py && flask run

tests:
	pytest

db:
	docker run -p 127.0.0.1:3306:3306 --name telagogod -e MARIADB_ROOT_PASSWORD=Admin123! -d mariadb:10.5