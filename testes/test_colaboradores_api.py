import unittest
from app import create_app
import json


class DepartamentoTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.db = self.app.db
        self.client = self.app.test_client

    def tearDown(self):
        from sqlalchemy_utils.functions import drop_database

        drop_database(self.app.config["SQLALCHEMY_DATABASE_URI"])

    def test_get_colaboradores(self):
        res = self.client().get("/colaboradores/")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            res.json,
            [],
        )

    def test_post_colaboradores(self):
        dict_departamento = {"name": "Departamento Testeiro"}
        res = self.client().post(
            "/departamentos/",
            data=json.dumps(dict_departamento),
            content_type="application/json",
        )
        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            res.json,
            {
                "success": f"""Departamento cadastrado - {dict_departamento.get("name")}"""
            },
        )
