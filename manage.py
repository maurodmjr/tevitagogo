#!/usr/bin/env python
from flask import Flask
from flask_restx import Api
from werkzeug.middleware.proxy_fix import ProxyFix
from app.instance.config import app_config


def create_app(config_name="development"):
    from app import create_app as c_app

    return c_app(config_name)


if __name__ == "__main__":
    app = create_app()
    app.run()
