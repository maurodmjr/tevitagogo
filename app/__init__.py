from flask import Flask
from flask_restx import Api
from werkzeug.middleware.proxy_fix import ProxyFix
from app.routes.departments import ns_dept
from app.routes.employees import ns_colab
from app.instance.config import app_config
from app.database.config import database_config
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def create_db(app):
    db.init_app(app)
    with app.app_context():
        from app.database.department_model import Department
        from app.database.employee_model import Employee

        db.create_all("__all__")

        return db


def create_app(config_name="development"):
    app = Flask("app")
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile("./instance/config.py")
    app.wsgi_app = ProxyFix(app.wsgi_app)

    api = Api(
        app,
        version="1.0",
        title="Docs ACMEVita",
        description="Desafio Técnico ACMEVita",
    )

    api.add_namespace(ns_dept)
    api.add_namespace(ns_colab)

    username, password, host, port, database = database_config.values()
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = f"mariadb+mariadbconnector://{username}:{password}@{host}:{port}/{database}"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    engine = create_engine(app.config["SQLALCHEMY_DATABASE_URI"])
    if not database_exists(engine.url):
        create_database(engine.url)

    db = create_db(app)
    app.db = db

    return app
