from werkzeug.exceptions import HTTPException


class ErroGenerico(HTTPException):
    code = 507
    description = "Ocorreu um erro inesperado"
