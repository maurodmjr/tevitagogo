from app.database.default_fields import DefaultFields
from app import db


class Department(db.Model, DefaultFields):
    __tablename__ = "departament"
    dept_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), unique=True)
