import os
from typing import Dict


database_config: Dict[str, str] = {
    "username": os.getenv("DATABASE_USER", "root"),
    "password": os.getenv("DATABASE_PASSWORD", "Admin123!"),
    "host": os.getenv("DATABASE_HOST", "0.0.0.0"),
    "port": os.getenv("DATABASE_PORT", "3306"),
    "database": os.getenv(
        "DATABASE_NAME",
        "telagogodb_test",
    ),
}
