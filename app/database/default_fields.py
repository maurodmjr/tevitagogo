from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class DefaultFields:
    created_at = db.Column(db.DateTime, default=datetime.now())
    last_modified = db.Column(
        db.DateTime, default=datetime.now(), onupdate=datetime.now()
    )
