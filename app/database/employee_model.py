from app.database.department_model import Department
from app.database.default_fields import DefaultFields
from app import db


class Employee(db.Model, DefaultFields):
    __tablename__ = "employee"
    employee_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    full_name = db.Column(db.String(250), index=True)
    dependentes = db.Column(db.String(300))
    dept_id = db.Column(db.Integer, db.ForeignKey(Department.dept_id), nullable=False)
