def insert_row(session, model, data):
    session.add(model(name=data.get("name")))
    session.commit()


def get_rows(session, model, data=None):
    query = session.query(model)
    if data:
        return query.filter_by(data)

    return query.all()
