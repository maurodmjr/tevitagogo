from flask_restx import Resource, fields, Namespace
from typing import Dict
from app.exceptions import ErroGenerico
from app.database import controllers


ns_dept = Namespace("departamentos", description="Manipulação de Departamentos")

departamento = ns_dept.model(
    "Departamento",
    {
        "name": fields.String(required=True, description="Nome do departamento"),
    },
)


@ns_dept.route("/")
class Departments(Resource):
    @ns_dept.doc("lista os departamentos")
    def get(self) -> Dict[str, str]:
        from app.database.department_model import Department

        return [
            {"nome": r.name, "id": r.id}
            for r in controllers.get_rows(self.api.app.db.session, Department)
        ]

    @ns_dept.doc("inserindo departamento")
    @ns_dept.expect(departamento)
    def post(self) -> Dict[str, str]:
        from app.database.department_model import Department

        try:
            controllers.insert_row(
                self.api.app.db.session, Department, self.api.payload
            )
        except:
            raise ErroGenerico

        return {
            "success": f"""Departamento cadastrado - {self.api.payload.get("name")}"""
        }
