from flask_restx import Resource, fields, Namespace
from typing import Dict
from app.database import controllers


ns_colab = Namespace("colaboradores", description="Manipulação de Colaboradores")

colaborador = ns_colab.model(
    "Colaborador",
    {
        "full_name": fields.String(required=True, description="Nome do colaborador"),
        "departamento": fields.List(fields.String, required=True, description="Departamento(s)"),
        "dependentes": fields.List(fields.String, description="Dependente(s)"),
    },
)


@ns_colab.route("/")
class Employees(Resource):
    @ns_colab.doc("lista os colaboradores")
    def get(self) -> Dict[str, str]:
        from app.database.employee_model import Employee

        return [
            {
                "nomeCompleto": r.full_name,
                "departamento": r.dept_id,
                "haveDependents": True if r.dependentes else False,
            }
            for r in controllers.get_rows(self.api.app.db.session, Employee)
        ]

    @ns_colab.doc("inserindo colaborador")
    @ns_colab.expect(colaborador)
    def post(self) -> Dict[str, str]:
        # vai chamar o controler que vai fazer os paranaues
        return {
            "success": f"""Departamento cadastrado - {self.api.payload.get("name")}"""
        }
